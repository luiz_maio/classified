<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolios extends Model
{
    protected $fillable = array(
        'id_usuario',
        'titulo',
		'descricao',
		'thumb'
	);

    public function usuarios()
    {
    	return $this->belongsTo('App\Usuarios', 'id_usuario');
    }
}
