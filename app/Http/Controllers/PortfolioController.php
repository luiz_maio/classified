<?php

namespace App\Http\Controllers;

use App\Portfolios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class PortfolioController extends Controller
{
    private $portfolio;

    public function __construct(Portfolios $portfolio)
    {
    	$this->portfolio = $portfolio;
    }

    public function getNovo()
    {
        return view('admin.portfolios.novo');
    }

    public function postNovo(Request $request)
    {
        if(empty($request->titulo) || empty($request->descricao)){
            return false;
        }

        $portfolio = $this->portfolio;

        if($portfolio->where('id_usuario', Auth::user()->id)->count() >= 12)
            return redirect()->back()->withErrors('Você já atingiu o limite de portfólios cadastrados. Edite ou remova um dos itens existentes.');

        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;
        $imagePath = '/images/portfolios/';

        $dados['id_usuario'] = Auth::user()->id;

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;
        }

        if($portfolio->create($dados))
            return redirect()->route("admin.home.index")->with('status', 'Portfólio cadastrado com sucesso!');
        else
            return redirect()->back()->withErrors('Erro no cadastro do portfólio. Tente novamente!');
    }

    public function getEdit($id)
    {
        $portfolio = $this->portfolio->find($id);
        return view('admin.portfolios.edit', compact('portfolio'));
    }

    public function postEdit($id, Request $request)
    {
        if(empty($request->titulo) || empty($request->descricao)){
            return false;
        }

        $portfolio = $this->portfolio->find($id);

        if($portfolio->id_usuario != Auth::user()->id)
            return redirect()->route("admin.home.index")->withErrors('Portfólio pertencente a outro usuário. Não é possível altera-lo.');

        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;
        $imagePath = '/images/portfolios/';

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;

            if(File::isFile(public_path().$portfolio->thumb)){
                File::delete(public_path().$portfolio->thumb);
            }
        }

        if($portfolio->update($dados))
            return redirect()->route("admin.home.index")->with('status', 'Portfólio alterado com sucesso!');
        else
            return redirect()->back()->withErrors('Erro na edição do portfólio. Tente novamente!');
    }

    public function getRemove($id)
    {
        $portfolio = $this->portfolio->find($id);

        if($portfolio->id_usuario != Auth::user()->id)
            return redirect()->route("admin.home.index")->withErrors('Portfólio pertencente a outro usuário. Não é possível remove-lo.');

        if(File::isFile(public_path().$portfolio->thumb)){
            File::delete(public_path().$portfolio->thumb);
        }

        $portfolio->delete();

        return redirect()->route("admin.home.index")->with('status', 'Portfólio removido com sucesso!');
    }
}
