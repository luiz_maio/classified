<?php

namespace App\Http\Controllers;

use App\Usuarios;
use App\Profissoes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class HomeController extends Controller
{
    private $usuario;
    private $ufs;

    public function __construct(Usuarios $usuario)
    {
    	$this->usuario = $usuario;
        $this->ufs = array('AC' => 'AC', 'AL' => 'AL', 'AP' => 'AP', 'AM' => 'AM', 'BA' => 'BA', 'CE' => 'CE', 'DF' => 'DF',
            'ES' => 'ES', 'GO' => 'GO', 'MA' => 'MA', 'MT' => 'MT', 'MS' => 'MS', 'MG' => 'MG', 'PR' => 'PR', 'PB' => 'PB',
            'PA' => 'PA', 'PE' => 'PE', 'PI' => 'PI', 'RJ' => 'RJ', 'RN' => 'RN', 'RS' => 'RS', 'RO' => 'RO', 'RR' => 'RR',
            'SC' => 'SC', 'SE' => 'SE', 'SP' => 'SP', 'TO' => 'TO'
        );
    }

    public function index()
    {
        $user = Usuarios::find(Auth::user()->id);
        return view('admin.home.index', compact('user'));
    }

    public function getEdit()
    {
        $user = $this->usuario->find(Auth::user()->id);
        $allJobs = Profissoes::all();
        $ufs = $this->ufs;
        $userJobs = array();

        foreach ($allJobs as $value) {
            $jobs[$value->id] = $value->nome;
        }

        foreach ($user->profissoes as $profissao) {
            $userJobs[] = $profissao->id;
        }

        $user['jobs'] = $userJobs;

        return view('admin.home.edit', compact('user', 'jobs', 'ufs'));
    }

    public function postEdit(Request $request)
    {
        if(empty($request->nome) || empty($request->sobrenome || empty($request->descricao))){
            return false;
        }

        $user = $this->usuario->find(Auth::user()->id);
        $dados = $request->all();

        if(isset($dados['email']))
            unset($dados['email']);

        if(isset($dados['senha'])){
            if($dados['senha'] != $dados['confirma_senha'])
                return redirect()->back()->withErrors('As senhas digitadas não conferem. Tente novamente!');
        }

        $dados['senha'] = bcrypt($dados['senha']);

        $image = isset($dados['image']) ? $dados['image'] : null;
        $imagePath = '/images/perfis/';

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;

            if(File::isFile(public_path().$user->thumb)){
                File::delete(public_path().$user->thumb);
            }
        }

        if(isset($dados['profissoes']))
    	   $user->profissoes()->sync($dados['profissoes']);
        else
            $user->profissoes()->detach();

        if($user->update($dados))
            return redirect()->route("admin.home.index")->with('status', 'Dados alterados com sucesso!');
        else
            return redirect()->back()->withErrors('Erro na edição dos dados. Tente novamente!');
    }
}
