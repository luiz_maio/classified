<?php

namespace App\Http\Controllers;

use Mail;
use Validator;
use App\Usuarios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function getLogin()
    {
        if(Auth::check()){
            return redirect('admin');
        }
        return view('admin.login');
    }

    public function postLogin(Request $request)
    {
        $rules = array(
			'email' => 'required|email',
			'senha' => 'required|min:6',
		);

		$messages = array(
	    	'email.required' => 'Digite o Email',
	    	'email.email' => 'Formato de Email Incorreto',
	    	'senha.required' => 'Senha Obrigatória',
	    	'senha.min' => 'A Senha deve conter no Minimo 6 Caracteres',
    	);

    	$validator = Validator::make($request->only('email', 'senha'), $rules, $messages);

        if (!$validator->passes()) {
            return redirect('/login')
                        ->withErrors($validator)
                        ->withInput();
        }

    	$access = $request->all();

	    $credentials = [
	        'email' => $access['email'],
	        'password' => $access['senha']
	    ];

	    if(Auth::attempt($credentials)){
            return redirect()->route('admin.home.index');
	    }
	    else{
	    	return redirect('/login')
                ->withErrors('Usuário não localizado. Verifique os dados de acesso e tente novamente.')
                ->withInput();
	    }
    }

    public function getForgot()
    {
        if(Auth::check()){
	    	return redirect('admin');
	    }
        return view('admin.forgot');
    }

    public function postForgot(Request $request)
    {
        $user = Usuarios::where('email', $request->email)->first();
        $update = array();

        if($user){
            $pass = $this->gerarSenha(8);
            $update['senha'] = bcrypt($pass);

            $data = array(
                'nome' => htmlentities($user->nome),
                'email' => $request->email,
                'senha' => $pass
            );

            if($user->update($update)){
                Mail::send('emails.new-pass', $data, function ($message) use($data) {
                    $message->from('ambev@paintpackred.com', 'Find Worker');
                    $message->to($data['email'])->subject('Recuperação de Senha');
                });
            }
        }
        else {
            return redirect()->route("admin.forgot")->withErrors('Email não cadastrado. Verifique e tente novamente!');
        }

        return redirect()->route("admin.login")->with('status', 'Senha enviada com sucesso. Verifique sua caixa de mensagens!');
    }

    public function gerarSenha($total_caracteres)
    {
        $caracteres = 'ABCDEFGHIJKLMNOPQRSTUWXYZ';
        $caracteres .= 'abcdefghijklmnopqrstuwxyz';
        $caracteres .= '0123456789';
        $caracteres .= '@#$&*';
        $max = strlen($caracteres)-1;
        $senha = null;
        for($i=0; $i < $total_caracteres; $i++){
            $senha .= $caracteres{mt_rand(0, $max)};
        }
        return $senha;
    }
}
