<?php

namespace App\Http\Controllers;

use App\Profissoes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class ProfissaoController extends Controller
{
    private $profissao;

    public function __construct(Profissoes $profissao)
    {
    	$this->profissao = $profissao;
    }

    public function index()
    {
        $jobs = $this->profissao->orderBy('nome')->get();
        $jobs = $this->paginate($jobs);

        return view('admin.profissoes.index', compact('jobs'));
    }

    public function search($name)
    {
        $jobs = $this->profissao->where('nome', 'like', '%' . $name . '%')->orderBy('nome')->get();
        $jobs = $this->paginate($jobs);

        return view('admin.profissoes.index', compact('jobs'));
    }

    public function getNovo()
    {
        return view('admin.profissoes.novo');
    }

    public function postNovo(Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

        $profissao = $this->profissao;
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;
        $imagePath = '/images/profissoes/';

        if($profissao->where('nome', $dados['nome'])->first())
            return redirect()->back()->withErrors('Já existe uma profissão ' . $dados['nome'] . ' cadastrada!');

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;
        }

        if($profissao->create($dados))
            return redirect()->route("admin.profissoes")->with('status', 'Profissão ' . $dados['nome'] . ' cadastrada com sucesso!');
        else
            return redirect()->back()->withErrors('Erro no cadastro da profissão. Tente novamente!');
    }

    public function getEdit($id)
    {
        $profissao = $this->profissao->find($id);
        return view('admin.profissoes.edit', compact('profissao'));
    }

    public function postEdit($id, Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

        $profissao = $this->profissao->find($id);
        $dados = $request->all();
        $image = isset($dados['image']) ? $dados['image'] : null;
        $imagePath = '/images/profissoes/';

        if($profissao->where('nome', $dados['nome'])->where('id', '<>', $id)->first())
            return redirect()->back()->withErrors('Já existe uma profissão ' . $dados['nome'] . ' cadastrada!');

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;

            if(File::isFile(public_path().$profissao->thumb)){
                File::delete(public_path().$profissao->thumb);
            }
        }

        if($profissao->update($dados))
            return redirect()->route("admin.profissoes")->with('status', 'Profissão ' . $dados['nome'] . ' alterada com sucesso!');
        else
            return redirect()->back()->withErrors('Erro na edição da profissão. Tente novamente!');
    }

    public function getRemove($id)
    {
        $profissao = $this->profissao->find($id);
        $nome = $profissao->nome;

        if(File::isFile(public_path().$profissao->thumb)){
            File::delete(public_path().$profissao->thumb);
        }

        $profissao->usuarios()->detach();
        $profissao->delete();

        return redirect()->route("admin.profissoes")->with('status', 'Profissão ' . $nome . ' removida com sucesso!');
    }

    public function paginate($value)
    {
        $perPage = 30;
        $collection = new Collection($value);
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageSearchResults = $collection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $results = new LengthAwarePaginator($currentPageSearchResults, count($collection), $perPage);
        $results->setPath('');

        return $results;
    }
}
