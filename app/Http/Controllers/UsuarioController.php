<?php

namespace App\Http\Controllers;

use Mail;
use Validator;
use App\Perfis;
use App\Usuarios;
use App\Profissoes;
use App\Portfolios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class UsuarioController extends Controller
{
    private $usuario;
    private $ufs;

    public function __construct(Usuarios $usuario)
    {
        $this->usuario = $usuario;
    	$this->ufs = array('AC' => 'AC', 'AL' => 'AL', 'AP' => 'AP', 'AM' => 'AM', 'BA' => 'BA', 'CE' => 'CE', 'DF' => 'DF',
            'ES' => 'ES', 'GO' => 'GO', 'MA' => 'MA', 'MT' => 'MT', 'MS' => 'MS', 'MG' => 'MG', 'PR' => 'PR', 'PB' => 'PB',
            'PA' => 'PA', 'PE' => 'PE', 'PI' => 'PI', 'RJ' => 'RJ', 'RN' => 'RN', 'RS' => 'RS', 'RO' => 'RO', 'RR' => 'RR',
            'SC' => 'SC', 'SE' => 'SE', 'SP' => 'SP', 'TO' => 'TO'
        );
    }

    public function index()
    {
        $users = Usuarios::where('id_perfil', '<>', 1)->orderBy('id_perfil')->orderBy('nome')->get();
        $users = $this->paginate($users);

        return view('admin.usuarios.index', compact('users'));
    }

    public function search($name)
    {
        $users = Usuarios::where('id_perfil', '<>', 1)
            ->where('nome', 'like', '%' . $name . '%')
            ->orWhere('sobrenome', 'like', '%' . $name . '%')
            ->orderBy('id_perfil')->orderBy('nome')->get();
        $users = $this->paginate($users);

        return view('admin.usuarios.index', compact('users'));
    }

    public function getNovo()
    {
        $allJobs = Profissoes::all();
        $allPerfis = Perfis::all();
        $ufs = $this->ufs;

        foreach ($allJobs as $job) {
            $jobs[$job->id] = $job->nome;
        }

        foreach ($allPerfis as $perfil) {
            $perfis[$perfil->id] = $perfil->nome;
        }

        if(Auth::user()->id_perfil != 1)
            unset($perfis[1]);

        return view('admin.usuarios.novo', compact('jobs', 'ufs', 'perfis'));
    }

    public function postNovo(Request $request)
    {
        if(empty($request->nome) || empty($request->sobrenome) || empty($request->email) || empty($request->descricao)){
            return false;
        }

        $user = $this->usuario;
        $dados = $request->all();
        $pass = $this->gerarSenha(8);

        if($user->where('email', $dados['email'])->first())
            return redirect()->back()->withErrors('Já existe um usuário cadastrado com esse email!')->withInput();

        $data = array(
            'nome' => htmlentities($dados['nome']),
            'email' => $dados['email'],
            'senha' => $pass
        );

        $dados['senha'] = bcrypt($pass);
        $image = isset($dados['image']) ? $dados['image'] : null;
        $imagePath = '/images/perfis/';

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;
        }

        if(isset($dados['profissoes']))
            $newUser = $user->create($dados)->profissoes()->sync($dados['profissoes']);
        else
            $newUser = $user->create($dados);

        if($newUser){
            Mail::send('emails.new-user', $data, function ($message) use($data) {
                $message->from('ambev@paintpackred.com', 'Find Worker');
                $message->to($data['email'])->subject('Cadastro de Usuário');
            });
        }
        else{
            return redirect()->back()->withErrors('Erro na criação de usuário. Tente novamente!');
        }

        return redirect()->route("admin.usuarios.index")->with('status', 'Usuário criado com sucesso!');
    }

    public function getEdit($id)
    {
        $user = $this->usuario->find($id);
        $allJobs = Profissoes::all();
        $allPerfis = Perfis::all();
        $ufs = $this->ufs;
        $userJobs = array();

        foreach ($allJobs as $value) {
            $jobs[$value->id] = $value->nome;
        }

        foreach ($user->profissoes as $profissao) {
            $userJobs[] = $profissao->id;
        }

        foreach ($allPerfis as $perfil) {
            $perfis[$perfil->id] = $perfil->nome;
        }

        if(Auth::user()->id_perfil != 1)
            unset($perfis[1]);

        $user['jobs'] = $userJobs;

        return view('admin.usuarios.edit', compact('user', 'jobs', 'perfis', 'ufs'));
    }

    public function postEdit($id, Request $request)
    {
        if(empty($request->nome) || empty($request->sobrenome || empty($request->descricao))){
            return false;
        }

        $user = $this->usuario->find($id);
        $dados = $request->all();

        if(isset($dados['email']))
            unset($dados['email']);

        $image = isset($dados['image']) ? $dados['image'] : null;
        $imagePath = '/images/perfis/';

        if(!is_null($image)){
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$imagePath;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['thumb'] = $imagePath.$imageName;

            if(File::isFile(public_path().$user->thumb)){
                File::delete(public_path().$user->thumb);
            }
        }

        if(isset($dados['profissoes']))
    	   $user->profissoes()->sync($dados['profissoes']);
        else
            $user->profissoes()->detach();

        if($user->update($dados))
            return redirect()->route("admin.usuarios.index")->with('status', 'Dados alterados com sucesso!');
        else
            return redirect()->back()->withErrors('Erro na edição dos dados. Tente novamente!');
    }

    public function getRemove($id)
    {
        $user = $this->usuario->find($id);
        $port = new Portfolios;

        if(File::isFile(public_path().$user->thumb)){
            File::delete(public_path().$user->thumb);
        }

        foreach ($user->portfolios as $portfolio) {
            $removePort = $port->find($portfolio->id);
            $removePort->delete();
        }

        $user->profissoes()->detach();
        $user->delete();

        return redirect()->route("admin.usuarios.index")->with('status', 'Usuário removido com sucesso!');
    }

    public function paginate($value)
    {
        $perPage = 30;
        $collection = new Collection($value);
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageSearchResults = $collection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $results = new LengthAwarePaginator($currentPageSearchResults, count($collection), $perPage);
        $results->setPath('');

        return $results;
    }

    public function gerarSenha($total_caracteres)
    {
        $caracteres = 'ABCDEFGHIJKLMNOPQRSTUWXYZ';
        $caracteres .= 'abcdefghijklmnopqrstuwxyz';
        $caracteres .= '0123456789';
        $caracteres .= '@#$&*';
        $max = strlen($caracteres)-1;
        $senha = null;
        for($i=0; $i < $total_caracteres; $i++){
            $senha .= $caracteres{mt_rand(0, $max)};
        }
        return $senha;
    }
}
