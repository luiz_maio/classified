<?php

namespace App\Http\Controllers;

use App\Usuarios;
use App\Profissoes;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class PagesController extends Controller
{
    private $profissoes;
    private $cities;

    public function __construct(Profissoes $works, Usuarios $users)
    {
        foreach ($works->all() as $value) {
            $profissoes[$value->id] = $value->nome;
        }

        foreach ($users->all() as $value) {
            $key = str_replace(' ', '-', strtolower(preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $value->end_cidade))));
            $cities[$key . '-' . $value->end_uf] = $value->end_cidade . '-' . $value->end_uf;
        }

        asort($cities);
        $cities = array_unique($cities);

        $this->cities = $cities;
        $this->profissoes = $profissoes;
    }

    public function index()
    {
        $cities = $this->cities;
        $profissoes = $this->profissoes;

        $allUsers = Usuarios::where('status', '1')->orderBy('nome')->get();

        foreach ($allUsers as $user) {
            $user->profissoes;
            $users[] = $user;
        }

        $users = $this->paginate($users);

        return view('pages.index', compact('users', 'cities', 'profissoes'));
    }

    public function search(Request $request)
    {
        $users = array();
        $jobSearch = array();
        $cities = $this->cities;
        $profissoes = $this->profissoes;

        $dados = $request->all();

        if(isset($dados['jobChosen']) && isset($dados['cityChosen'])){
            if($dados['jobChosen'] == '0')
                unset($dados['jobChosen']);
            if($dados['cityChosen'] == '0')
                unset($dados['cityChosen']);

            if(isset($dados['jobChosen'])){
                $jobChosen = $dados['jobChosen'];
                $jobSearch = Profissoes::find($jobChosen);
            }
            else
                $jobChosen = '';
            $ufChosen = isset($dados['cityChosen']) ? substr($dados['cityChosen'], -2) : '';
            $cityChosen = isset($dados['cityChosen']) ? str_replace('-', ' ', substr($dados['cityChosen'], 0, -3)) : '';

            if(isset($dados['jobChosen']) && isset($dados['cityChosen']))
                $allUsers = Usuarios::join('usuario_profissoes', 'usuarios.id', '=', 'usuario_profissoes.id_usuario')
                    ->where('usuario_profissoes.id_profissao', $jobChosen)
                    ->where('usuarios.end_cidade', 'like', "$cityChosen")
                    ->where('usuarios.end_uf', 'like', "$ufChosen")
                    ->where('usuarios.status', '1')
                    ->orderBy('usuarios.nome')
                    ->select('usuarios.*')
                    ->distinct()
                    ->get();
            elseif(isset($dados['jobChosen']) && !isset($dados['cityChosen']))
                $allUsers = Usuarios::join('usuario_profissoes', 'usuarios.id', '=', 'usuario_profissoes.id_usuario')
                    ->where('usuario_profissoes.id_profissao', $jobChosen)
                    ->where('usuarios.status', '1')
                    ->orderBy('usuarios.nome')
                    ->select('usuarios.*')
                    ->distinct()
                    ->get();
            elseif(!isset($dados['jobChosen']) && isset($dados['cityChosen']))
                $allUsers = Usuarios::where('end_cidade', 'like', "$cityChosen")
                ->where('end_uf', 'like', "$ufChosen")
                ->where('status', '1')
                ->orderBy('nome')
                ->get();
            else
                return redirect()->route('pages.index');
        }
        elseif(isset($dados['workerName'])){
            $allUsers = Usuarios::where('nome', 'like', '%' . $dados['workerName'] . '%')
            ->orWhere('sobrenome', 'like', '%' . $dados['workerName'] . '%')
            ->orderBy('nome')
            ->get();
        }
        else {
            $allUsers = Usuarios::where('status', '1')->orderBy('nome')->get();
        }

        foreach ($allUsers as $user) {
            $user->profissoes;
            $users[] = $user;
        }

        return view('pages.search', compact('users', 'cities', 'profissoes', 'jobSearch'));
    }

    public function worker($id)
    {
        $cities = $this->cities;
        $profissoes = $this->profissoes;

        $user = Usuarios::find($id);
        $user->portfolios;
        $user->profissoes;
        return view('pages.worker', compact('user', 'cities', 'profissoes'));
    }

    public function paginate($value)
    {
        $perPage = 10;
        $collection = new Collection($value);
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentPageSearchResults = $collection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $results = new LengthAwarePaginator($currentPageSearchResults, count($collection), $perPage);
        $results->setPath('');

        return $results;
    }
}
