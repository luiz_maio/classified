<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuarios extends Authenticatable
{
    protected $fillable = array(
        'id_perfil',
        'nome',
		'sobrenome',
		'email',
		'senha',
		'thumb',
		'descricao',
        'fone_fixo',
		'fone_cel',
        'end_logradouro',
        'end_numero',
        'end_complemento',
        'end_bairro',
        'end_cidade',
        'end_uf',
        'end_cep',
        'status'
	);

    public function getAuthPassword()
    {
    	return $this->senha;
    }

    public function perfis()
    {
    	return $this->belongsTo('App\Perfis', 'id_perfil');
    }

    public function portfolios()
    {
    	return $this->hasMany('App\Portfolios', 'id_usuario');
    }

    public function profissoes()
    {
    	return $this->belongsToMany('App\Profissoes', 'usuario_profissoes', 'id_usuario', 'id_profissao');
    }
}
