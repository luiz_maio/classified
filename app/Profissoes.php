<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profissoes extends Model
{
    protected $fillable = array(
        'nome',
        'thumb'
	);

    public function usuarios()
    {
    	return $this->belongsToMany('App\Usuarios', 'usuario_profissoes', 'id_profissao', 'id_usuario');
    }
}
