<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', array('as' => 'pages.index', 'uses' => 'PagesController@index'));
Route::get('/worker/search', array('as' => 'pages.search', 'uses' => 'PagesController@search'));
Route::get('/worker/{id}', array('as' => 'pages.worker', 'uses' => 'PagesController@worker'));


Route::get('/login', array('as' => 'admin.login', 'uses' => 'LoginController@getLogin'));
Route::post('/login', array('as' => 'admin.login.post', 'uses' => 'LoginController@postLogin'));
Route::get('/forgot', array('as' => 'admin.forgot', 'uses' => 'LoginController@getForgot'));
Route::post('/forgot', array('as' => 'admin.forgot.post', 'uses' => 'LoginController@postForgot'));

Route::get('/logout', function() {
    Auth::logout();
    return Redirect::to('/login');
});

// Rotas de Usuários Autenticados
Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/', array('as' => 'admin.home.index', 'uses' => 'HomeController@index'));
        Route::get('/edit', array('as' => 'admin.home.edit', 'uses' => 'HomeController@getEdit'));
        Route::post('/edit', array('as' => 'admin.novo.edit.post', 'uses' => 'HomeController@postEdit'));
        Route::get('/portfolios/novo', array('as' => 'admin.portfolios.novo', 'uses' => 'PortfolioController@getNovo'));
        Route::post('/portfolios/novo', array('as' => 'admin.portfolios.novo.post', 'uses' => 'PortfolioController@postNovo'));
        Route::get('/portfolios/edit/{id}', array('as' => 'admin.portfolios.edit', 'uses' => 'PortfolioController@getEdit'));
        Route::post('/portfolios/edit/{id}', array('as' => 'admin.portfolios.edit.post', 'uses' => 'PortfolioController@postEdit'));
        Route::get('/portfolios/remove/{id}', array('as' => 'admin.portfolios.remove', 'uses' => 'PortfolioController@getRemove'));
        Route::group(['middleware' => 'admin'], function () {
            Route::get('/usuarios', array('as' => 'admin.usuarios.index', 'uses' => 'UsuarioController@index'));
            Route::get('/usuarios/search/{name}', array('as' => 'admin.usuarios.search', 'uses' => 'UsuarioController@search'));
            Route::get('/usuarios/novo', array('as' => 'admin.usuarios.novo', 'uses' => 'UsuarioController@getNovo'));
            Route::post('/usuarios/novo', array('as' => 'admin.usuarios.novo.post', 'uses' => 'UsuarioController@postNovo'));
            Route::get('/usuarios/edit/{id}', array('as' => 'admin.usuarios.edit', 'uses' => 'UsuarioController@getEdit'));
            Route::post('/usuarios/edit/{id}', array('as' => 'admin.usuarios.edit.post', 'uses' => 'UsuarioController@postEdit'));
            Route::get('/usuarios/remove/{id}', array('as' => 'admin.usuarios.remove', 'uses' => 'UsuarioController@getRemove'));
            Route::get('/profissoes', array('as' => 'admin.profissoes', 'uses' => 'ProfissaoController@index'));
            Route::get('/profissoes/search/{name}', array('as' => 'admin.profissoes.search', 'uses' => 'ProfissaoController@search'));
            Route::get('/profissoes/novo', array('as' => 'admin.profissoes.novo', 'uses' => 'ProfissaoController@getNovo'));
            Route::post('/profissoes/novo', array('as' => 'admin.profissoes.novo.post', 'uses' => 'ProfissaoController@postNovo'));
            Route::get('/profissoes/edit/{id}', array('as' => 'admin.profissoes.edit', 'uses' => 'ProfissaoController@getEdit'));
            Route::post('/profissoes/edit/{id}', array('as' => 'admin.profissoes.edit.post', 'uses' => 'ProfissaoController@postEdit'));
            Route::get('/profissoes/remove/{id}', array('as' => 'admin.profissoes.remove', 'uses' => 'ProfissaoController@getRemove'));
        });
        Route::group(['middleware' => 'master'], function () {

        });
    });
});
