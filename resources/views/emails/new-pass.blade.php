<table width="500" cellspacing="0" cellpadding="30" border="0" style="margin: 0 auto;">
	<tr>
		<td>
			<b>TROCAR O EMAIL SMTP!!!</b>
		</td>
	</tr>
	<tr>
		<td width="500" style="font-size:15px; color:#777777; font-family: Arial, Verdana, Tahoma;">
			Ol&aacute; {{ $nome }}, <br /><br />
			Sua senha foi alterada! <a href="{{ url('/login') }}" style="text-decoration:none; color: #777777" target="_blank">
			<b>Clique Aqui</b></a> e acesse o sistema com os dados abaixo: <br /><br />
			<b>Login:</b> {{ $email }} <br />
			<b>Senha:</b> {{ $senha }} <br /><br />
			<small><b>OBS:</b> Por motivos de seguran&ccedil;a, aconselhamos a altera&ccedil;&atilde;o assim que poss&iacute;vel!</small> <br /><br />
			Att,<br /><br />
			Find Workers
		</td>
	</tr>
</table>
