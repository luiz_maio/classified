<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="HandheldFriendly" content="true"/>
        <meta name="MobileOptimized" content="320"/>
        <meta http-equiv="cleartype" content="on">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

        <title>Find Worker @yield('title')</title>

        <!-- Scripts -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.25.8/js/jquery.tablesorter.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.min.js"></script>
        <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

        <!-- Bootstrap core CSS -->
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet">
        <link href="//cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.min.css" rel="stylesheet">
        <link href="{{ asset('/css/main.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/responsive.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/zoom.portfolio.css') }}" rel="stylesheet">

        <script src="{{ asset('/js/hover.zoom.js') }}"></script>
        <script src="{{ asset('/js/hover.zoom.conf.js') }}"></script>
        <script src="{{ asset('/js/zoom.portfolio.js') }}"></script>
        <script src="{{ asset('/js/select.itens.js') }}"></script>

    </head>

  <body>
      <!-- Static navbar -->
      <div class="navbar navbar-inverse navbar-static-top">
        <div class="container">
          <div style="width: 50%; float: left;">
            <a href="/">
                <img src="{{ asset('/images/comum/home.png') }}" alt="Início" title="Início">
            </a>
          </div>
          <div style="width: 50%; float: right; margin-top: 10px;">
              <div id="search">
                  <a href="admin">Acesso Administrativo</a>
                  <input type="text" name="box-search" id="box-search" size="40" placeholder="Busca por Nome..." />
              </div>
          </div>
          {!! Form::open(['route' => 'pages.search', 'method' => 'get', 'class' => 'filter-search']) !!}
              <h4 style="text-align: center;">Filtros de Busca</h4>
              <select name="jobChosen" class="jobChosen">
                  <option value="0">Selecione uma profissão...</option>
                  @if(count($profissoes) > 0)
                      @foreach ($profissoes as $key => $value)
                          <option value="{{ $key }}">{{ $value }}</option>
                      @endforeach
                  @endif
              </select>
              <select name="cityChosen" class="cityChosen">
                  <option value="0">Selecione uma cidade...</option>
                  @if(count($cities) > 0)
                      @foreach ($cities as $key => $value)
                          <option value="{{ $key }}">{{ $value }}</option>
                      @endforeach
                  @endif
              </select>
              <div class="search-button">
                  <button type="submit" class="btn btn-success">BUSCAR PROFISSIONAIS</button>
              </div>
          {!! Form::close() !!}

        </div>
      </div>

    @yield('content')

  	<div id="footer">
  		<div class="container">
  			<div class="row">

  			</div>
  		</div>
  	</div>
    </body>
  </html>
