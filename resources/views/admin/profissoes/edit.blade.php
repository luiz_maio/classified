@extends('admin-template')

@section('title')
	- Editar Profissão
@stop

@section('content')

	<div class="messages">
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
					{{ $error }}<br />
				@endforeach
			</div>
		@endif
	</div>

    <div class="list-title-header">
    	<h1>Editar Profissão</h1>
    </div>

	<div class="">
		{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package', 'files' => 'true')) !!}
			<div class="form-group">
				{!! Form::label('lblNome', 'Nome', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('nome', $profissao->nome, array('class' => 'form-control', 'id' => 'editNome')) !!}
		    	</div>
		    </div>
			<div class="form-group" id="listImage">
				{!! Form::label('lblThumb', 'Ícone', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
					<table id="list-tipos-logos" class="table">
						<tr>
							<td class="col-sm-2">
							{{ Html::image($profissao->thumb ? $profissao->thumb : asset('/images/comum/noImage.png'), 'alt', array('class' => 'img-rounded img-logo')) }}
							</td>
						</tr>
						<tr>
							<td class="col-sm-2">
								{!! Form::file('image') !!}
							</td>
						</tr>
					</table>
				</div>
			</div>
    		<div class="modalButtons">
				{{ Html::link('admin/profissoes', 'Cancelar', array('class' => 'btn btn-info')) }}
			    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
			</div>
		{!! Form::close() !!}
	</div>

@stop
