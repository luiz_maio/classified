@extends('admin-template')

@section('title')
	- Profissões
@stop

@section('content')

	<script src="{{ asset('/js/admin.js') }}"></script>
    <script>
    	$(function () {
    		$('#btn-search').click(function () {
    			var src = $('#txt-search').val();
    			src.trim() == '' ? window.location = "<?php echo url('admin/profissoes'); ?>" : window.location = "<?php echo url('admin/profissoes/search'); ?>/" + src;
    		});
    	});
    </script>

	<div class="messages">
		@if(session('status'))
			<div class="alert alert-success">
				{{ session('status') }}
			</div>
		@endif
	</div>

    <div class="list-title-header">
    	<h1><span class="glyphicon glyphicon-briefcase"></span> Profissões</h1>
    </div>

    <div class="list-header">
    	<div>
    		<div class="input-group col-xs-6 search">
    			<input type="text" id="txt-search" class="form-control" placeholder="Buscar por Profissão ...">

    			<span class="input-group-btn">
    				<button id="btn-search" class="btn btn-default" type="button">Buscar</button>
    			</span>
    		</div>

    		<!-- Trigger the modal with a button -->
    		{{ Html::link('admin/profissoes/novo', 'Nova Profissão', array('class' => 'btn btn-info novo-item')) }}

    	</div>
    </div>

    <div class="table-responsive clear">

        @if (count($jobs) >= 1)
        	<table id="list-users" class="table table-striped">
        		<thead>
        			<tr>
        				<th>Nome</th>
        				<th>Ações</th>
        			</tr>
        		</thead>
        		<tbody>
        			@foreach($jobs as $job)
        				<tr>
        					<td>{{ $job->nome }}</td>
        					<td>
                                <a href="profissoes/edit/{{ $job->id }}" id="btn-edit" class="btn btn-primary btn-sm btn-edit">
	    							<span class="glyphicon glyphicon-edit"></span> Editar
	    						</a>
	                            <a href="profissoes/remove/{{ $job->id }}" id="btn-remove" class="btn btn-danger btn-sm btn-danger" onclick="return confirm('Deseja remover essa profissão?');">
	    							<span class="glyphicon glyphicon-trash"></span> Remover
	    						</a>
        					</td>
        				</tr>
        			@endforeach
        		</tbody>
        	</table>
			<div class="lista-paginacao">
	            {!! $jobs->render() !!}
	        </div>
        @else
        	<div>
        		<h4>Nenhuma Profissão Localizada</h4>
        	</div>
        @endif

    </div>

@stop
