@extends('admin-template')

@section('title')
	- Usuários
@stop

@section('content')

    <script src="{{ asset('/js/admin.js') }}"></script>
    <script>
    	$(function () {
    		$('#btn-search').click(function () {
    			var src = $('#txt-search').val();
    			src.trim() == '' ? window.location = "<?php echo url('admin/usuarios'); ?>" : window.location = "<?php echo url('admin/usuarios/search'); ?>/" + src;
    		});
    	});
    </script>

	<div class="messages">
		@if(session('status'))
			<div class="alert alert-success">
				{{ session('status') }}
			</div>
		@endif
	</div>

    <div class="list-title-header">
    	<h1><span class="glyphicon glyphicon-user"></span> Usuários</h1>
    </div>

    <div class="list-header">
    	<div>
    		<div class="input-group col-xs-6 search">
    			<input type="text" id="txt-search" class="form-control" placeholder="Buscar por Nome ...">

    			<span class="input-group-btn">
    				<button id="btn-search" class="btn btn-default" type="button">Buscar</button>
    			</span>
    		</div>

    		<!-- Trigger the modal with a button -->
    		{{ Html::link('admin/usuarios/novo', 'Novo Usuário', array('class' => 'btn btn-info novo-item')) }}

    	</div>
    </div>

    <div class="table-responsive clear">

        @if (count($users) >= 1)
        	<table id="list-users" class="table table-striped">
        		<thead>
        			<tr>
        				<th>Nome</th>
        				<th>Bairro</th>
        				<th>Cidade</th>
                        <th>UF</th>
        				<th>Perfil</th>
        				<th>Status</th>
        				<th>Ações</th>
        			</tr>
        		</thead>
        		<tbody>
        			@foreach($users as $user)
        				<tr>
        					<td>{{ $user->nome . ' ' . $user->sobrenome }}</td>
        					<td>{{ $user->end_bairro }}</td>
                            <td>{{ $user->end_cidade }}</td>
        					<td>{{ $user->end_uf }}</td>
                            <td>{{ $user->perfis->nome }}</td>
        					<td>{{ $user->status ? 'Ativo' : 'Inativo' }}</td>
        					<td>
								@if(Auth::user()->id_perfil == 1 || (Auth::user()->id_perfil == 2 && $user->id_perfil != 2))
	                                <a href="usuarios/edit/{{ $user->id }}" id="btn-edit" class="btn btn-primary btn-sm btn-edit">
		    							<span class="glyphicon glyphicon-edit"></span> Editar
		    						</a>
		                            <a href="usuarios/remove/{{ $user->id }}" id="btn-remove" class="btn btn-danger btn-sm btn-danger" onclick="return confirm('Deseja remover esse usuário?');">
		    							<span class="glyphicon glyphicon-trash"></span> Remover
		    						</a>
								@endif
        					</td>
        				</tr>
        			@endforeach
        		</tbody>
        	</table>
			<div class="lista-paginacao">
	            {!! $users->render() !!}
	        </div>
        @else
        	<div>
        		<h4>Nenhum Usuário Localizado</h4>
        	</div>
        @endif

    </div>

@stop
