<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Find Worker - Nova Senha</title>

    <!-- Scripts -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>

    <!-- CSS -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/login.css') }}" rel="stylesheet">

    <!-- Icone -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/images/common/icon.png') }}" />

</head>
<body>

    <div class="wrapper">
        {!! Form::open(array('class' => 'form-signin', 'id' => 'form-package')) !!}
            <h2 class="form-signin-heading">Recuperar Senha</h2>
            <div class="messages">                              
                @if (count($errors) > 0)
                    <div class="alert-error">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br />
                        @endforeach
                    </div>
                @endif
            </div>
            {!! Form::text('email', '', array('class' => 'form-control', 'id' => 'editEmail', 'placeholder' => 'Email', 'required', 'autofocus')) !!}
            {!! Form::submit('Enviar Nova Senha', array('class' => 'btn btn-lg btn-success btn-block')) !!}
            <div>{{ Html::link('/login', 'Voltar', array('class' => 'forgot-password')) }}</div>
        {!! Form::close() !!}
    </div>

</body>
</html>
