@extends('admin-template')

@section('title')
	- Editar Conta de Usuário
@stop

@section('content')

	<script src="{{ asset('/js/home/edit.js') }}"></script>

	<div class="messages">
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
					{{ $error }}<br />
				@endforeach
			</div>
		@endif
	</div>

    <div class="list-title-header">
		<a href="javascript:void(0);" id="changePass">Alterar Senha</a>
    	<h1>Editar Conta de Usuário</h1>
    </div>

	<div class="">
		{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package', 'files' => 'true')) !!}
			<div class="form-group">
				{!! Form::label('lblNome', 'Nome*', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-5">
					{!! Form::text('nome', $user->nome, array('class' => 'form-control', 'id' => 'editNome')) !!}
				</div>
				{!! Form::label('lblSobrenome', 'Sobrenome*', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-5">
					{!! Form::text('sobrenome', $user->sobrenome, array('class' => 'form-control', 'id' => 'editSobrenome')) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('lblEmail', 'Email', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-5">
					{!! Form::text('email', $user->email, array('class' => 'form-control', 'id' => 'editEmail', 'readonly')) !!}
				</div>
				{!! Form::label('lblStatus', 'Status', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-5">
					{!! Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), $user->status, array('class' => 'form-control', 'id' => 'editStatus')) !!}
				</div>
			</div>
			<div class="form-group change-pass">
				{!! Form::label('lblSenha', 'Senha', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-5">
			    	{!! Form::password('senha', array('class' => 'form-control', 'id' => 'editSenha')) !!}
		    	</div>
				{!! Form::label('lblSenha', 'Confirmar Senha', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-5">
			    	{!! Form::password('confirma_senha', array('class' => 'form-control', 'id' => 'editConfirmaSenha')) !!}
		    	</div>
			</div>
			<div class="form-group">
				{!! Form::label('lblFone', 'Telefone', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-5">
					{!! Form::text('fone_fixo', $user->fone_fixo, array('class' => 'form-control', 'id' => 'editFone', 'placeholder' => '(11)2345-6789')) !!}
				</div>
				{!! Form::label('lblFoneCel', 'Celular', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-5">
					{!! Form::text('fone_cel', $user->fone_cel, array('class' => 'form-control', 'id' => 'editFoneCel', 'placeholder' => '(11)98765-4321')) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('lblLogradouro', 'Endereço', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-5">
					{!! Form::text('end_logradouro', $user->end_logradouro, array('class' => 'form-control', 'id' => 'editLogradouro')) !!}
				</div>
				{!! Form::label('lblNum', 'Num.', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-1">
					{!! Form::text('end_numero', $user->end_numero, array('class' => 'form-control', 'id' => 'editNum')) !!}
				</div>
				{!! Form::label('lblComplemento', 'Compl.', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::text('end_complemento', $user->end_complemento, array('class' => 'form-control', 'id' => 'editComplemento')) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('lblBairro', 'Bairro', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::text('end_bairro', $user->end_bairro, array('class' => 'form-control', 'id' => 'editBairro')) !!}
				</div>
				{!! Form::label('lblCidade', 'Cidade', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-2">
					{!! Form::text('end_cidade', $user->end_cidade, array('class' => 'form-control', 'id' => 'editCidade')) !!}
				</div>
				{!! Form::label('lblUF', 'UF', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-1">
					{!! Form::select('end_uf', $ufs, $user->end_uf, array('class' => 'form-control', 'id' => 'editUF')) !!}
				</div>
				{!! Form::label('lblCEP', 'CEP', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-2">
					{!! Form::text('end_cep', $user->end_cep, array('class' => 'form-control', 'id' => 'editCEP')) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('lblDescricao', 'Descrição*', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
					{!! Form::textarea('descricao', $user->descricao, array('class' => 'form-control', 'id' => 'editDescricao',
						'placeholder' => 'Descrição pessoal de no máximo 500 caracteres.', 'maxlength' => '500')) !!}
				</div>
			</div>
			<div class="form-group" id="listImage">
				{!! Form::label('lblThumb', 'Imagem', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-3">
					<table id="list-tipos-logos" class="table">
						<tr>
							<td class="col-sm-2">
								{{ Html::image($user->thumb ? $user->thumb : asset('/images/comum/noImage.png'), 'alt', array('class' => 'img-rounded img-logo')) }}
							</td>
						</tr>
						<tr>
							<td class="col-sm-2">
								{!! Form::file('image') !!}
							</td>
						</tr>
					</table>
				</div>
				{!! Form::label('lblModulos', 'Profissões', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-7">
					<div class="form-control col-sm-7" style="width: 100%; height: 110px; padding: 5px 10px; overflow: auto;">
						@foreach($jobs as $key => $job)
							<div class="col-sm-4" style="border: 1px solid #ccd0d2; border-radius: 4px; padding: 3px;">
								{!! Form::checkbox('profissoes[]', $key, in_array($key, $user['jobs']) ? true : null, array('id' => 'editModulo'.$key)) !!}
								{{ $job }}
							</div>
						@endforeach
					</div>
				</div>
			</div>

			<div class="form-group">
				(*) Itens Obrigatórios
			</div>

			<div class="modalButtons">
				{{ Html::link('admin/', 'Cancelar', array('class' => 'btn btn-info')) }}
			    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
			</div>
		{!! Form::close() !!}
	</div>

@stop
