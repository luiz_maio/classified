@extends('admin-template')

@section('title')
	- Home
@stop

@section('content')

	<div class="messages">
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
					{{ $error }}<br />
				@endforeach
			</div>
		@endif
		
		@if(session('status'))
			<div class="alert alert-success">
				{{ session('status') }}
			</div>
		@endif
	</div>

    <div class="list-title-header">
    	<h1 class="titulo-action">Pré-Visualização</h1>
		{{ Html::link('admin/edit', 'Alterar Dados', array('class' => 'btn btn-info edit-user-home')) }}
    </div>

	<div id="ww">
	    <div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 centered">
                    <img class="circle" src="{{ $user->thumb }}" title="{{ $user->nome . ' ' . $user->sobrenome }}" alt="{{ $user->nome . ' ' . $user->sobrenome }}">
					<h1>{{ $user->nome . ' ' . $user->sobrenome }}</h1>
					<p>
                        {{ $user->descricao }}
                    </p>
                    <h4>Contatos</h4>
                    <p>
                        @if(!empty($user->fone_fixo))
                            Fone Fixo: {{ $user->fone_fixo }}</br>
                        @endif
                        @if(!empty($user->fone_cel))
                            Fone Celular: {{ $user->fone_cel }}</br>
                        @endif
                        @if(!empty($user->email))
                            Email: {{ $user->email }}</br>
                        @endif
                        @if(!empty($user->end_bairro))
                            Bairro: {{ $user->end_bairro . ' - ' . $user->end_cidade . ' - ' . $user->end_uf }}</br>
                        @endif
                            Profissão(ões):
                        @foreach ($user->profissoes as $key => $profissao)
                            @if($key != 0)
                                /
                            @endif
                                {{ $profissao->nome }}
                        @endforeach
                    </p>
				</div>
			</div>
	    </div>
	</div>

	<div class="list-title-header title-portfolio">
		<h1 class="titulo-action">Portfólio</h1>
		@if (count($user->portfolios) <= 12)
			{{ Html::link('admin/portfolios/novo', 'Novo Portfólio', array('class' => 'btn btn-info edit-user-home')) }}
		@endif
    	<h4>Inclua até 12 portfólios com fotos e descrições de serviços prestados</h4>
    </div>

	<div class="table-responsive clear portfolio">

        @if (count($user->portfolios) >= 1)
        	<table id="list-users" class="table table-striped">
        		<thead>
        			<tr>
						<th>#</th>
        				<th>Imagem</th>
						<th>Título</th>
        				<th>Ações</th>
        			</tr>
        		</thead>
        		<tbody>
					<?php $count = 1; ?>
        			@foreach($user->portfolios as $portfolio)
        				<tr>
							<td>{{ $count }}</td>
							<td><img src="{{ $portfolio->thumb }}" title="{{ $portfolio->titulo }}" alt="{{ $portfolio->titulo }}"></td>
        					<td>{{ $portfolio->titulo }}</td>
        					<td>
                                <a href="admin/portfolios/edit/{{ $portfolio->id }}" id="btn-edit" class="btn btn-primary btn-sm btn-edit">
	    							<span class="glyphicon glyphicon-edit"></span> Editar
	    						</a>
	                            <a href="admin/portfolios/remove/{{ $portfolio->id }}" id="btn-remove" class="btn btn-danger btn-sm btn-danger" onclick="return confirm('Deseja remover esse portifólio?');">
	    							<span class="glyphicon glyphicon-trash"></span> Remover
	    						</a>
        					</td>
        				</tr>
						<?php $count++; ?>
        			@endforeach
        		</tbody>
        	</table>
        @else
        	<div>
        		<h4>Nenhum Portfolio Cadastrado</h4>
        	</div>
        @endif

    </div>

@stop
