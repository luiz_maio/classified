@extends('pages-template')

@section('title')
    @if(isset($user->nome) && isset($user->sobrenome))
        - {{ $user->nome . ' ' . $user->sobrenome }}
    @endif
@stop

@section('content')

    <div id="ww">
	    <div class="container">
            @if(!empty($jobSearch))
                <div class="search-by-job">
                    <img src="{{ $jobSearch->thumb ? $jobSearch->thumb : asset('/images/comum/job.png') }}" alt="Profissão" title="Profissão">
                    <div class="">
                        <h2>{{ $jobSearch->nome }}</h2>
                    </div>
                </div>
            @else
    			<div class="row">
                    <div class="worker-box-search worker-box-sample">
                        <div class="img-worker-box">
                            <img src="{{ asset('/images/comum/person.png') }}" alt="Nome do Profissional" title="Nome do Profissional">
                        </div>
                        <div class="data-worker-box">
                            <b>Nome do Profissional</b><br />
                            <small>
                                Bairro - Cidade - UF<br />
                                Profissão(ões)
                            </small>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <div class="container pt">
        <div class="box-search">
            @if(count($users) > 0)
                <?php $count = 1; ?>
                <div class="worker-box-row">
                    @foreach ($users as $key => $user)
                        <div class="worker-box-search">
                            <div class="img-worker-box">
                                <img src="{{ $user->thumb }}" alt="{{ $user->nome . ' ' . $user->sobrenome }}" title="{{ $user->nome . ' ' . $user->sobrenome }}">
                            </div>
                            <a href="{{ URL::route('pages.worker', ['id' => $user->id]) }}">
                                <div class="data-worker-box">
                                    <b>{{ $user->nome . ' ' . $user->sobrenome }}</b><br />
                                    <small>
                                        {{ $user->end_bairro . ' - ' . $user->end_cidade . ' - ' . $user->end_uf }}<br />
                                        @foreach ($user->profissoes as $key => $profissao)
                                            @if($key != 0)
                                                /
                                            @endif
                                                {{ $profissao->nome }}
                                        @endforeach
                                    </small>
                                </div>
                            </a>
                        </div>
                        @if($count % 2 == 0)
                            </div>
                            <div class="worker-box-row">
                        @endif
						<?php $count++; ?>
                    @endforeach
                </div>
            @endif
        </div>
    </div>

@stop
