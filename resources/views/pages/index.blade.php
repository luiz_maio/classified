@extends('pages-template')

@section('title')
	@if(isset($user->nome) && isset($user->sobrenome))
        - {{ $user->nome . ' ' . $user->sobrenome }}
    @endif
@stop

@section('content')

    <div id="ww">
	    <div class="container">
			<div class="row">
                <div class="worker-box-search">
                    <p>
                        Página para busca de profissionais prestadores de serviço de sua região.<br />
                        Utilize os filtros de busca no topo da página, ou busque o profissional pelo nome.
                    </p>
                    <p>
                        Se você é um prestador de serviços e tem interesse em se cadastrar, entre em contato com o
                        administrador do sistema através do email fulano@ciclano.com
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="container pt">
        <div class="box-search">
            @if(count($users) > 0)
				<?php $count = 1; ?>
                <div class="worker-box-row">
                    @foreach ($users as $key => $user)
                        <div class="worker-box-search">
                            <div class="img-worker-box">
                                <img src="{{ $user->thumb }}" alt="{{ $user->nome . ' ' . $user->sobrenome }}" title="{{ $user->nome . ' ' . $user->sobrenome }}">
                            </div>
                            <a href="{{ URL::route('pages.worker', ['id' => $user->id]) }}">
                                <div class="data-worker-box">
                                    <b>{{ $user->nome . ' ' . $user->sobrenome }}</b><br />
                                    <small>
                                        {{ $user->end_bairro . ' - ' . $user->end_cidade . ' - ' . $user->end_uf }}<br />
                                        @foreach ($user->profissoes as $key => $profissao)
                                            @if($key != 0)
                                                /
                                            @endif
                                                {{ $profissao->nome }}
                                        @endforeach
                                    </small>
                                </div>
                            </a>
                        </div>
                        @if($count % 2 == 0)
                            </div>
                            <div class="worker-box-row">
                        @endif
						<?php $count++; ?>
                    @endforeach
                </div>
            @endif
        </div>
        <div class="lista-paginacao">
            {!! $users->render() !!}
        </div>
    </div>

@stop
