@extends('pages-template')

@section('title')
    @if(isset($user->nome) && isset($user->sobrenome))
        - {{ $user->nome . ' ' . $user->sobrenome }}
    @endif
@stop

@section('content')
	<div id="ww">
	    <div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 centered">
                    <img class="circle" src="{{ $user->thumb }}" title="{{ $user->nome . ' ' . $user->sobrenome }}" alt="{{ $user->nome . ' ' . $user->sobrenome }}">
					<h1>{{ $user->nome . ' ' . $user->sobrenome }}</h1>
					<p>
                        {{ $user->descricao }}
                    </p>
                    <h4>Contatos</h4>
                    <p>
                        @if(!empty($user->fone_fixo))
                            Fone Fixo: {{ $user->fone_fixo }}</br>
                        @endif
                        @if(!empty($user->fone_cel))
                            Fone Celular: {{ $user->fone_cel }}</br>
                        @endif
                        @if(!empty($user->email))
                            Email: {{ $user->email }}</br>
                        @endif
                        @if(!empty($user->end_bairro))
                            Bairro: {{ $user->end_bairro . ' - ' . $user->end_cidade . ' - ' . $user->end_uf }}</br>
                        @endif
                            Profissão(ões):
                        @foreach ($user->profissoes as $key => $profissao)
                            @if($key != 0)
                                /
                            @endif
                                {{ $profissao->nome }}
                        @endforeach
                    </p>
				</div>
			</div>
	    </div>
	</div>

	<div class="container pt">
		<div class="row mt centered zoom-gallery">
            @if(count($profissoes) > 0)
                @foreach ($user->portfolios as $portfolio)
                    <div class="img-portfolio">
                        <a class="zoom green" href="{{ $portfolio->thumb }}" title="{{ $portfolio->titulo }}" data-source="{{ $portfolio->descricao }}">
                            <img src="{{ $portfolio->thumb }}" alt="{{ $portfolio->titulo }}">
                        </a>
                        <p>{{ $portfolio->titulo }}</p>
                    </div>
                @endforeach
            @else
                <div>
                    Não há portfólio cadastrado para esse profissional.
                </div>
            @endif
		</div>
	</div>
@stop
