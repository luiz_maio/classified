$(function() {
    $(".cityChosen, .jobChosen").chosen();

    $('#box-search').keyup(function(e){
        if(e.keyCode == 13)
        {
            $(location).attr("href", window.location.origin + '/worker/search?workerName=' + $(this).val());
        }
    });
});
