$(function() {
    $("form").validate({
		rules:{
			nome:{
				required: true
			},
			sobrenome:{
				required: true
			},
			descricao: {
                required: true
            },
            id_perfil:{
				required: true
			}
		},
		messages:{
			nome:{
				required: "Digite o nome"
			},
			sobrenome:{
				required: "Digite o sobrenome"
			},
			descricao:{
				required: "Digite uma descrição pessoal"
			},
			id_perfil:{
				required: "Selecione um perfil"
			}
		}
	});

    $("#editFone").mask("(99) 9999-9999");
    $("#editFoneCel").mask("(99) 99999-9999");
    $('.change-pass').hide();
    $('.change-pass').prop( "disabled", true );

    $('#changePass').click(function () {
        if($('.change-pass').css('display') == 'block'){
            $('.change-pass').hide();
            $('#editSenha').val('');
            $('#editConfirmaSenha').val('');
            $('.change-pass').prop( "disabled", true );
            $('#changePass').html('Alterar Senha');
        }else {
            $('.change-pass').show();
            $('.change-pass').prop( "disabled", false );
            $('#changePass').html('Não Alterar Senha');
        }
    });
});
