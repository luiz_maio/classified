<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $perfis = [
            ['nome' => 'Master'],
            ['nome' => 'Admin'],
            ['nome' => 'User']
        ];

        DB::table('perfis')->insert($perfis);

        $profissoes = [
            'nome' => 'Auxiliar de Serviços Gerais',
            'thumb' => '/images/profissoes/82028c264852c4af79ab242464ca5708878d02d6.png'
        ];

        DB::table('profissoes')->insert($profissoes);

        $usuarios = [
            'id_perfil' => 1,
            'nome' => 'Fernando',
            'sobrenome' => 'Maio',
            'email' => 'maio.fernando@gmail.com',
            'senha' => bcrypt('Mon$on08'),
            'thumb' => '/images/perfis/master.png',
            'status' => 1
        ];

        DB::table('usuarios')->insert($usuarios);
    }
}
